$('#carousel-quotes').carousel();

$('#carousel-projects').carousel({
	interval: 2000
})

function removeParams() {
	history.pushState("", document.title, window.location.href.split('?')[0]);
}

function moreProjects(){
	var content = "<div class='row row-loadable'>"+
	"<div class='projects-gallery-single-sm single-up single-left'></div>"+
	"<div class='projects-gallery-single-sm single-down single-left'></div>"+
	"<div class='projects-gallery-single-md'></div>"+
	"<div class='projects-gallery-single-sm single-up single-right'></div>"+
	"<div class='projects-gallery-single-sm single-down single-right'></div>"+
	"</div>"+
	"<div class='projects-btn center'>"+
	"<a class='lab-btn lab-btn-big lab-btn-white projects-btn-more' onclick='moreProjects()''>VER MÁS PROYECTOS</a>"+
	"</div>"
	$('.projects-btn').remove()
	$('.projects-gallery').append(content)
}

function hideNewsletter(){
	$('.newsletter').animate(
		{opacity: 0}, 500,
		function(){$('.newsletter').addClass('hidden'); }
	);
}

$(document).ready( function() {
	if (window.location.hash == '#sent') {
		$('.newsletter-form').remove();
		$('.newsletter-info').remove();
		$('.newsletter').addClass('sent').append("<div class='newsletter-sent'>TE HAS SUSCRITO AL NEWSLETTER</div>")
	}
	if (window.location.hash == '#wrongEmail') {
		$('#email').attr('placeholder', 'Ingrese un email válido')
	}
	removeParams();
});

function sendNewsletter(){
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	if(emailReg.test($('#email').val())) {
		window.location.hash = "#sent";
	}
	else {
		window.location.hash = "#wrongEmail";
	}
}