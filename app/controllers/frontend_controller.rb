# FrontendController -> Controller out the back-office
class FrontendController < ApplicationController
  layout 'layouts/frontend/application', except: :wireframe
  before_filter :set_metas

  def index
  end

  def services
  end

  def team
  end

  def projects
  end

  def contact_us
  end

  def identity
  end

  def web
  end

  def socials
  end

  def blog
    
  end

  private

  def set_metas
    @meta = MetaTag.get_by_url(request.url)
    @google_adword = GoogleAdword.get_by_url(request.url)
  end
end
